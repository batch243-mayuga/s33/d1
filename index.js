// [Section] JavaScript Synchronous vs Asynchronous
// JavaScript is by default synchronous means that only one statement can be executed at a time

console.log("Hello World");
// conssole.log("Hello again");
console.log("Goodbye");

// code blocking


// Asynchronous means that we can proceed to execute other statements, while time consuming code are running in the background


// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
// Syntax
	//fetch('URL')
	// .then((response) =>{})

// [CHECK THE STATUS OF THE REQUEST]
// By using the .then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response=>console.log(response.status));

// The "fetch" method will return a promise that evolves to a "response" object
// The .then method captures the "Response" object and returns another "promise" which will eventually be "resolved" or "rejected"

// [RETRIEVE CONTENTS/DATA FROM THE RESPONSE OBJECT]

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())

// Using multiple "then" method creates a "promise chain"
.then((json) => console.log(json))

// [CREATE A FUNCTION THAT WILL DEMONSTRATE USING 'async' and 'await' keywords]

// The 'async' and 'await' keywords is another approach that can be used to achieve an asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates an synchronous function

async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the result variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	// Result returned by fetch is a "promise"
	console.log(result);

	// The returned "response" is an object
	console.log(typeof result);

	// We can not access the content of the "Response" directly by accessing its body property
	console.log(result.body)

	// Converts the data from the "response" object as JSON
	let json = await result.json();

	console.log(json);

}
fetchData();

// [RETRIEVE A SPECIFIC POST]

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response)=>response.json())
	.then((json)=> console.log(json))


// [CREATE POST]
	// SYNTAX:
	// fetch('URL', options)
	// .then((response)=>{})
	// .then((response)=>{})

	fetch('https://jsonplaceholder.typicode.com/posts',{

		// Sets the method of the "Request" object to POST
		method: 'POST',

		//Specified that the content will be in a JSON structure
		headers: {
			'Content-type' : 'application/json'
		},

		body: JSON.stringify({
			title: 'New Post',
			body: "Hello World!",
			userId: 1
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// [UPDATE A POST USING PUT METHOD]

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			id:1,
			title: 'Updated post',
			body: "Hello again!",
			userId: 1
		})
	})
	.then((response)=> response.json())
	.then((json)=> console.log(json));	


// [UPDATE A POST USING PATCH METHOD]

	// PATCH is used to update the whole object
	// PUT is used to update a single property

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-type':'application/json'
		},
		body: JSON.stringify({
			title: 'Corrected post'
		})
	})
	.then((response)=> response.json())
	.then((json)=> console.log(json));	

// [DELETE A POST]

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "DELETE"
	})